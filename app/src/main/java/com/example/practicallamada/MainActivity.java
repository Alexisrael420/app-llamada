package com.example.practicallamada;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private EditText txtContrasenia;
    private Button btnIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUsuario = (EditText) findViewById(R.id.editTextTextUsuario);
        txtContrasenia = (EditText) findViewById(R.id.editTextContrasenia);

        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Usuario = txtUsuario.getText().toString();
                String Contrasenia = txtContrasenia.getText().toString();
                if(Usuario.equals("Alexis") && Contrasenia.equals("12345")){
                    Intent abrir = new Intent(MainActivity.this, Segundo_Activity.class);
                    startActivity(abrir);
                }else {
                    Toast.makeText(MainActivity.this, "USUARIO O CONTRASEÑA INCORRECTOS", Toast.LENGTH_LONG).show();
                }

            }
        });


    }
}