package com.example.practicallamada;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Segundo_Activity extends AppCompatActivity {

    private TextView textoNumero;
    private ImageButton btnLlamar2;
    private ImageButton btnLlamar1;
    private ImageButton btnBuscar;
    private Button btnCerrarSesion;

    private final int PHONE_CALL_CODE = 100;
    private final int CAMERA_CALL_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo_);

        textoNumero = (TextView) findViewById(R.id.textViewPersona1);
        btnLlamar1 = (ImageButton) findViewById(R.id.btnPersona1);
        btnLlamar2 = (ImageButton) findViewById(R.id.btnPersona2);
        btnCerrarSesion = (Button) findViewById(R.id.btnCerrar);

        btnLlamar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = textoNumero.getText().toString();
                if(num != null){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        //Toast.makeText(Segundo_Activity.this, "LLLLLLLLLL", Toast.LENGTH_SHORT).show();
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else {
                        versionesAnteriores(num);
                    }
                }
            }
            private void versionesAnteriores (String num){
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("telefono" + num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }else{
                    Toast.makeText(Segundo_Activity.this, "CONFIGURA PERMISOS", Toast.LENGTH_LONG);
                }

            }


        });
        btnLlamar2.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = textoNumero.getText().toString();
                if(num != null){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        //Toast.makeText(Segundo_Activity.this, "LLLLLLLLLL", Toast.LENGTH_SHORT).show();
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                    }else {
                        versionesAnteriores(num);
                    }
                }
            }
            private void versionesAnteriores (String num){
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("telefono" + num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }else{
                    Toast.makeText(Segundo_Activity.this, "CONFIGURA PERMISOS", Toast.LENGTH_LONG);
                }

            }
        }));
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent salir = new Intent(Segundo_Activity.this, MainActivity.class);
                startActivity(salir);
            }
        });
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       // Toast.makeText(this, "AAAAAAA", Toast.LENGTH_SHORT).show();
       switch (requestCode){
            case PHONE_CALL_CODE:
             String permission = permissions[0];
             int result = grantResults[0];

             if(permission.equals(Manifest.permission.CALL_PHONE)){

                 if(result == PackageManager.PERMISSION_GRANTED){

                     String phoneNumber = textoNumero.getText().toString();
                     Intent llamada = new Intent(Intent.ACTION_CALL,Uri.parse("tel:" + phoneNumber));
                     if (ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                     startActivity(llamada);
                 }else{
                     Toast.makeText(this,"Permiso No Aceptado", Toast.LENGTH_LONG).show();
                 }
             }
             break;
         case CAMERA_CALL_CODE:

              break;
         default:
             super.onRequestPermissionsResult(requestCode, permissions, grantResults);
             break;
       }

    }

    private boolean verificarPermisos (String permisos){
        int resultado = this.checkCallingOrSelfPermission(permisos);
        return  resultado == PackageManager.PERMISSION_GRANTED;
    }


}